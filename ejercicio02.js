function ComparaPrecio(a, b) {
    if (a.Preu === b.Preu) return 0;
    if (a.Preu > b.Preu) return 1;
    return -1;
}

function MenorQue3000KmYMarcaHonda(el) {
    el.model = el.model.toLowerCase();
    return (el.kilometres <= 30000 && el.model.includes("honda"));
}

function MenorQue30000KmYMas240CC(el) {
    return (el.kilometres <= 30000 && el.cilindrada > 240);
}

function MenorQue25000KmYMas350CCYPrecioEntre1800Y2200(el) {
    return (el.kilometres < 25000 && el.cilindrada > 350 && el.preu > 1800 && el.preu < 2200);
}

MOTOS.sort(ComparaPrecio);
console.log("La moto mas barata es " + MOTOS[0].model + " y la mas cara " + MOTOS[MOTOS.length - 1].model);

let MotosMenorQue3000KmYMarcaHonda = MOTOS.filter(MenorQue3000KmYMarcaHonda);
console.log(MotosMenorQue3000KmYMarcaHonda.length);

let MotosMenorQue3000KmYMas240CC = MOTOS.filter(MenorQue30000KmYMas240CC);
console.log(MotosMenorQue3000KmYMas240CC.length);

let MotosMenorQue25000KmYMas350CCYPrecioEntre1800Y2200 = MOTOS.filter(MenorQue25000KmYMas350CCYPrecioEntre1800Y2200);
console.log(MotosMenorQue25000KmYMas350CCYPrecioEntre1800Y2200.length);

var marca = [];

MOTOS.forEach(element => {
    let NombreMarca = element.model.split(" ")[0];
    let repetido = false;
    let ItemRepeated = 0;
    for (let i = 0; i < marca.length; i++) {
        if (NombreMarca === marca[i].nombre) {
            repetido = true;
            ItemRepeated = i;
        }
    }
    if (!repetido) {
        marca.push({
            nombre: NombreMarca,
            cantidad: 0
        });
        ItemRepeated = marca.length - 1;
    }
    marca[ItemRepeated].cantidad++;
});
console.log(marca);
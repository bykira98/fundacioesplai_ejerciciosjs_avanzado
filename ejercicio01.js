class Figura {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

class Rectangulo extends Figura {
    constructor(x, y, lado1, lado2) {
        super(x, y);
        this.lado1 = lado1;
        this.lado2 = lado2;
    }
    area() {
        return this.lado1 * this.lado2;
    }
}

class Triangulo extends Figura {
    constructor(x, y, base, altura) {
        super(x, y);
        this.base = base;
        this.altura = altura;
    }
    area() {
        return (this.base * this.altura) / 2;
    }
}

class Cuadrado extends Rectangulo {
    constructor(x, y, lado1) {
        super(x, y, lado1, lado1);
    }
    area(lado1, lado2) {
        return super.area(lado1, lado2);
    }
}

Rectangulo1 = new Rectangulo(1, 1, 12, 16);
Triangulo1 = new Triangulo(1, 1, 12, 28);
Cuadrado1 = new Cuadrado(1, 1, 6);

console.log(Rectangulo1.area());
console.log(Cuadrado1.area());
console.log(Triangulo1.area());
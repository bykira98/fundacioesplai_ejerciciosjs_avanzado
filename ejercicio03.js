function OrdenaEstacionesPorNumeroDeBicis(a, b) {
    if (a.free_bikes === b.free_bikes) return 0;
    if (a.free_bikes > b.free_bikes) return 1;
    return -1;
}

function EstacionesSinBicicleatasLibres(el) {
    return (el.free_bikes === 0);
}

function OrdenaEstacionesMasCercanasAPosicion(a, b) {
    let DifenciaEntrePosicionEstacionYObjetivoLatitudeA = Math.abs(a.latitude - 41.388163);
    let DifenciaEntrePosicionEstacionYObjetivoLongitudeA = Math.abs(a.longitude - 2.179769);
    let DifenciaEntrePosicionEstacionYObjetivoLatitudeB = Math.abs(b.latitude - 41.388163);
    let DifenciaEntrePosicionEstacionYObjetivoLongitudeB = Math.abs(b.longitude - 2.179769);
    if (DifenciaEntrePosicionEstacionYObjetivoLatitudeA === DifenciaEntrePosicionEstacionYObjetivoLatitudeB && DifenciaEntrePosicionEstacionYObjetivoLongitudeA === DifenciaEntrePosicionEstacionYObjetivoLongitudeB) return 0;
    if (DifenciaEntrePosicionEstacionYObjetivoLatitudeA > DifenciaEntrePosicionEstacionYObjetivoLatitudeB && DifenciaEntrePosicionEstacionYObjetivoLongitudeA > DifenciaEntrePosicionEstacionYObjetivoLongitudeB) return 1;
    return -1;
}
$.getJSON(
    "https://api.citybik.es/v2/networks/bicing",
    function (data) {
        let estacions = data.network.stations;
        estacions.sort(OrdenaEstacionesPorNumeroDeBicis);
        console.log(estacions[0]);
        var EstacionesSinBicis = estacions.filter(EstacionesSinBicicleatasLibres);
        console.log(EstacionesSinBicis);
        estacions.sort(OrdenaEstacionesMasCercanasAPosicion);
        console.log(estacions);
    });